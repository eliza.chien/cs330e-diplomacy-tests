
from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, army


class TestDiplomacy (TestCase):
    

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB London Support A\nC Beijing Move Madrid\nD Shanghai Support C\nE Tokyo Move Shanghai\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB London\nC [dead]\nD [dead]\nE [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Move Shanghai\nB Shanghai Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Shanghai\nB Madrid\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Support C\nB Barcelona Support D\nC London Move Barcelona\nD Venice Support B\nE Vienna Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Barcelona\nC [dead]\nD Venice\nE [dead]\n")

        

# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()


